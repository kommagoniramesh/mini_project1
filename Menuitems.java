package miniproject;

public class Menuitems {
	private int itemID;
	private String itemname;
	private int itemQuantity;
	private double itemPrice;
	
	public Menuitems(int itemID, String itemname, int itemQuantity, double itemPrice) throws IllegalArgumentException{
		super();
		if(itemID<0)
		{
			throw new IllegalArgumentException("exception occured, id cannot less than zero");
		}
		this.itemID = itemID;
		
		if(itemname==null) {
			throw new IllegalArgumentException("exception occured, name cannot be null");

		}
		this.itemname = itemname;
		if(itemQuantity<0)
		{
			throw new IllegalArgumentException("exception occured, quantity cannot less than zero");
		}
		
		this.itemQuantity = itemQuantity;
		
		if(itemPrice<0)
		{
			throw new IllegalArgumentException("exception occured, price cannot less than zero");
		}
		this.itemPrice = itemPrice;
	}

	public int getitemID() {
		return itemID;
	}

	public void setitemID(int itemID) {
		this.itemID = itemID;
	}

	public String getitemname() {
		return itemname;
	}

	public void setitemname(String itemname) {
		this.itemname = itemname;
	}

	public int getitemQuantity() {
		return itemQuantity;
	}

	public void setitemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public double getitemPrice() {
		return itemPrice;
	}

	public void setitemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	@Override
	public String toString() {
		return "" + itemID + " " + itemname + " " + itemQuantity + " "+ itemPrice  ;
	}
	

}