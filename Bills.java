package miniproject;
import java.util.Date;
import java.util.List;
public class Bills {
	private String name;
	private List<Menuitems> items;
	private double cost;
	private Date time;
	
	public Bills() {}
	
	public Bills(String name, List<Menuitems> items, double cost, Date time) throws IllegalArgumentException {
		super();
		
		this.name = name;
		this.items = items;
		if(cost<0)
		{
			throw new IllegalArgumentException("exception occured, cost cannot less than zero");
		}
		this.cost = cost;
		this.time = time;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Menuitems> getItems() {
		return items;
	}
	public void setItems(List<Menuitems> selectedItems) {
		this.items = selectedItems;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public Date getTime() {
		return time;
	}
	public void setTime(Date date) {
		this.time = date;
	}

}

	


